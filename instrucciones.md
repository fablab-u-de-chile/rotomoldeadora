# Instrucciones de uso

Antes de hacer el proceso de rotomoldeado, es necesario contar con un molde (con ciertas consideraciones de diseño) y un material cuya solidificación dependa únicamente del tiempo.

1. Preparar el molde, aplicar desmoldantes.
2. Remover el perfil portamolde de la máquina usando las perillas
3. Apernar la base del molde en el portamolde.
4. Preparar y vertir la mezcla.
5. Cerrar el molde.
6. Disponer el portamolde en el marco interno, usando las perilas nuevamente.
7. Encender la máquina usando el switch posterior.
8. Seleccionar una velocidad usando la perilla del potenciómetro.
9. Esperar a que el material solidifique.
10. Bajar la velocidad y apagar la máquina.
11. Retirar el portamolde y quitar el molde.
12. Volver a poner el portamolde.



