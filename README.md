# Bio rotomoldeadora

[![CC BY 4.0][cc-by-sa-shield]][cc-by-sa]

Bio rotomoldeadora es una máquina rotomoldeadora para fabricar geometrías cerradas y huecas de biomaterial. Es importante considerar que la deshidratación del biomaterial posterior al rotomoldeado hará que disminuya el tamaño de la geometría obtenida. El flujo de trabajo se inicia con la Biomixer, donde se generan mezclas precisas que luego se vierten en el molde que girará de forma controlada. 

Este proyecto, es parte de la iniciativa https://gitlab.com/fablab-u-de-chile/NBD liderada por el FabLab U. de Chile, financiado por el Ministerio de Culturas, Artes y Patrimonio de Chile, apoyado por la plataforma internacional Materiom, y desarrollado en colaboración con estudiantes e investigadores de la Universidad de Chile.

<img src="/IMG_5190__1_.jpg" width="430"> <img src="/Chanchito.png" width="430">

## Atributos

- Transmisión por correa.
- Estructura de perfiles T-Slot.
- Pocas partes impresas.
- Relacíon de rotación en marco externo a interno de 1:3
- Soporte ajustable para moldes, de T-Slot 20x20.
- No cuenta con calefacción del molde, los materiales deben solidifcar por acción del tiempo o de enfriamiento.
- Longitud interna del marco interno de 270 mm.

Instrucciones de uso [AQUÍ](instrucciones.md).

## Cómo construirlo

- Las lista de partes se encuentran en el siguiente archivo [AQUÍ](https://docs.google.com/spreadsheets/d/1oyjsB6tSy8h8sunW1DCzYqkn4mI8LXAf2roK5sLdBoA/edit?usp=sharing)
- Los archivos CAD (.f3z y .STEP) están en la carpeta parts.

<img src="/img/isorot.png" width="500">

## Trabajo futuro

- Actualizar repositorio de electrónica.
- Incluir componentes del controlador en el CAD.
- Incluir instrucciones de ensamble.

### Ponte en contacto

Puedes ver más de nuestra labor en:

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
